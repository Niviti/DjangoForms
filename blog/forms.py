
from django import forms
from .models import Post

"""
 user  = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    title = models.CharField(max_length=120)
    slug  = models.SlugField(unique=True)
    image = models.FileField(upload_to=upload_location, 
            null=True, 
            blank=True, 
            #width_field="width_field", 
            #height_field="height_field"
            )
    height_field = models.IntegerField(default=0)
    width_field = models.IntegerField(default=0)
    content = models.TextField()
    draft = models.BooleanField(default=False)
    publish = models.DateField(auto_now=False, auto_now_add=False)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
"""
class PostModelForm(forms.ModelForm):

        class Meta:
            model = Post
            ## jeśli coś jest autocreated w modelu nie mozemy tego wsadzić do fields
            ## w modelu moze byc 10 rzeczy ale wyswietli sie tylko to co jest w fields
            fields = [
                "user",
                "title",
                "slug",
                "image"
               ]
            ## Pola których nie chcemy
            ##exclude = ["height_field", "width_fields"]   
            ## deklaracja labels help_text error_messages to wszystko możemy zadeklarować właśnie tutaj
            labels = {
                  "title": "this is title label",
                  "slug": "This is slug" 
            }
            help_text = {
                "title": "this is litle label",
                "slug": "This is slug"
            }
            error_messages = {
                #  "title": {
                #     "max_legth": "This title is too long.",
                #     "required": "the title field is required."
                #  },
                  "slug": {
                     "max_legth": "This title is too long.",
                     "required": "the slug field is required.",
                #     "unique": "the slug must be unique."
                  },
            }

        def __init__(self, *args, **kwargs):
            
            super(PostModelForm, self).__init__(*args, **kwargs)
            ## rownie dobrze mozemy zadeklarowac pola help_text w __init__ wszystko zalezy od uzytkownika
            self.fields["title"].widget = forms.Textarea()
            self.fields["title"].error_messages = {
                     "max_legth": "This title is too long.",
                     "required": "the slug field is required."
            }
            self.fields["slug"].error_messages = {
                     "max_legth": "This title is too long.",
                     "required": "the slug field is required.",
                     "unique": "the slug field is required."
            }
            
            for field in self.fields.values():
                field.error_messages = {
                'required': "You Know, {fieldname} is required".format(fieldname=field.label), 
                }

        ## walidacja
        def clean_title(self, *args, **kwargs):
             title = self.cleaned_data.get("title")    
             return title    
        ## Skoro korzystamy z ModelForm trzeba to zapisać do bazy danych
        def save(self, commit=True, *args, **kwargs):     
             obj = super(PostModelForm, self).save(commit=False, *args, **kwargs)
             ## dodajemy dodatkowe pole
             obj.publish = "2016-10-01"
             obj.content = "Coming Soon"
             # from django.utils.text import slugify
             # obj.title = slugify(obj.title)
             ## zapisujemy obiekt do bazy danych
             if commit:
                 obj.save()
             return obj


SOME_CHOICES = [
    ('db-value', 'Display Value' ),
    ('db-value2', 'Display Value' ),
    ('db-value3', 'Display Value' ),
]

INTS_CHOICES = [tuple([x,x]) for x in range(0, 102)]

YEARS = [x for x in range(1980, 2030)]

 ## Walidacja jest juz w tym zrobiona !! 
class TestForm(forms.Form):
    ## mozemy tutaj zrobic walidacje min_length=10 
    ## initial="2010-01-20" to będzie w formsie na początku
    ## widget robi to co widzimy :)
    year_field = forms.DateField(initial="2010-01-20", widget=forms.SelectDateWidget(years=YEARS))
    some_text = forms.CharField(label='Text', widget=forms.Textarea(attrs={"rows":4, "cols": 10 }))
    choices = forms.CharField(label='Text', widget=forms.Select(choices=SOME_CHOICES))
    boolean = forms.BooleanField()
    integer = forms.IntegerField(initial=10, widget=forms.Select(choices=INTS_CHOICES))
    email = forms.EmailField(min_length=10)
    

    ## tutaj definiujemy co się dzieje na początku deklaracji formsa
    def __init__(self, new_text=None, *args, **kwargs):
           super(TestForm, self).__init__(*args, **kwargs)
           if new_text:
                    ## jeśli definiujemy form przez konstruktor to tutaj deklarowane jest pole
                    self.fields["some_text"].initial = new_text

    ## w ten sposób deklarujemy własną walidacje
    def clean_integer(self, *args, **kwargs):
        integer = self.cleaned_data.get("integer")
        if integer <10:
             raise forms.ValidationError("Podaj większą wartość")
        return integer
    
    ## deklaracja własnej walidacji
    def clean_some_text(self, *args, **kwargs):
        some_text = self.cleaned_data.get("some_text")
        if len(some_text) < 10:
             raise forms.ValidationError("Ensure the text is greater than 10 characters")


      
