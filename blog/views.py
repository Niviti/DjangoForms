from django.shortcuts import render

# Create your views here.
from django.forms import formset_factory, modelformset_factory
from django.utils import timezone
from .forms import  TestForm, PostModelForm

def formset_view(request):
    TestFormset = formset_factory(TestForm,extra=3)
    formset = TestFormset()
    
    if formset.is_valid():
    
      context = {
        "formset": formset
      }

    return render(request, "formset_view.html", context)

def home(request):

    form = PostModelForm(request.POST or None)
    ## jeżeli jest valid to cisniemy
    if form.is_valid():
         ## commit False nie zapisuj jeszcze tych danych
         obj = form.save(commit=False)
         print(obj.title)
         obj.title = "Some random title"
         obj.publish =  timezone.now()
         ## teraz po zmianie wysyłamy to co chcemy
         obj.save()
    
    ## jeśli error 
    if form.has_error:
            #print(form.errors.as_json())     
            #print(form.errors.as_text())
            
            # Bardzo spoko rzecz tak mozemy podejrzeć wszystkie mozliwe metody do wykorzystania
            print(dir(form.fields))        

    return render(request, 'forms.html', {"form": form } )